package main

import (
	"awesomeProject/utils"
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {

	type Uidphone struct {
		Uid int64
		Phone int64
	}

	type TmpUidPhone struct {
		Uid		string	`json:"uid"`
		Phone 	string	`json:"phone"`
		Info 	map[string]interface{}	`json:"info"`
	}
	// Set client options
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		log.Fatal(err)
	}
	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		log.Fatal(err)
	}

	collection := client.Database("datagrin").Collection("uidphones")

	fmt.Println("Connected to MongoDB!")

	// Read directory
	files, err := utils.FilePathWalkDir("./data")
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		fmt.Println("Running in file: ", file)
		file, err := os.Open(file)

		if err != nil {
			log.Fatal(err)
		}

		defer file.Close()

		scanner := bufio.NewScanner(file)
		buf := make([]byte, 0, 64*1024)
		scanner.Buffer(buf, 1024*1024)
		for scanner.Scan() {
			line := scanner.Text()

			var jsonLine map[string]interface{}

			json.Unmarshal([]byte(line), &jsonLine)

			var uid = jsonLine["id"]

			uid, err := strconv.ParseInt(uid.(string), 10, 64)

			if err != nil {
				log.Fatal(err)
			}

			var uidphone Uidphone
			var tmpUidPhone	TmpUidPhone

			err = collection.FindOne(context.TODO(), bson.M{"uid": uid}).Decode(&uidphone)

			if err != nil {
				log.Fatal(err)
			}

			// Remove id
			delete(jsonLine, "id")

			tmpUidPhone.Phone = strings.Replace(strconv.FormatInt(uidphone.Phone, 10), "84", "0", 1)
			tmpUidPhone.Uid = strconv.FormatInt(uidphone.Uid, 10)
			tmpUidPhone.Info = jsonLine

			jsonUidPhone, err := json.Marshal(&tmpUidPhone)

			if err != nil {
				log.Fatal(err)
			}

			fmt.Println(string(jsonUidPhone))
			err = utils.WriteToFile("result.json", string(jsonUidPhone) + "\n")

			if err != nil {
				log.Fatal(err)
			}
			return
		}

		if err := scanner.Err(); err != nil {
			log.Fatal(err)
		}
	}
}
