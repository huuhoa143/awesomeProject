package utils

import (
	"os"
	"path/filepath"
)

func WriteToFile(file_name string, data string) error {
	file, err := os.OpenFile(file_name, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)

	if err != nil {
		return  err
	}

	defer file.Close()

	if _, err := file.WriteString(data); err != nil {
		return err
	}

	return file.Sync()
}

func FilePathWalkDir(root string) ([]string, error) {
	var files []string
	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			files = append(files, path)
		}
		return  nil
	})

	return  files, err
}